/*Variable pour simplifier le code*/
let btnCpt=document.getElementById("btnStart");
let btnStop=document.getElementById("btnStop");
let btnReset=document.getElementById("btnReset");
let txtTps=document.getElementById("txtTps");

/*Gestion d'évènements*/
btnCpt.addEventListener("click", btnStart);
btnStop.addEventListener("click", bntStop);
btnReset.addEventListener("click", reset);

/*variables globales*/
let cpt=null; // gestion d'affichage (setInterval)
let hdep=null; // heure de départ du chrono

/*méthode*/
function btnStart(){
    if(cpt !=null) return; // Si cpt différent de null ne fait rien car chrono déjà démarré
    hdep = new Date();
    cpt = setInterval(affHeure,1);/*moins il y a de zéro plus ça ira vite*/
    /*Gestion de l'interface homme machine*/
    btnStart.disabled=true;
    btnStop.disabled=false;
    btnReset.disabled=true;

}

function bntStop(){
    if(cpt == null) return; //chrono non démarré

    clearInterval(cpt);
    cpt = null;
    /*Gestion de l'IHM*/
    btnStart.disabled=true;
    btnStop.disabled=true;
    btnReset.disabled=false;
}

function reset() {
    if(cpt != null) return;//chrono démarré
    txtTps.value = "000:00:00:000";

    /*Gestion de l'IHM*/
    btnStart.disabled=false;
    btnStop.disabled=true;
    btnReset.disabled=true;
}

function affHeure(){
    let diff = new Date() - hdep;
    let tps = new Date(diff);
    /*amélioration de l'affichage avec toString et padStart*/
    let str = tps.getUTCHours().toString().padStart(3,"0") + ":"
        +tps.getUTCMinutes().toString().padStart(2,"0")+":"
        +tps.getUTCSeconds().toString().padStart(2,"0")+"."
        +tps.getUTCMilliseconds().toString().padStart(3,"0");
    txtTps.value = str;
}