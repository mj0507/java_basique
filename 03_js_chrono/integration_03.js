/*Variable pour simplifier le code*/
let $btnCpt=document.getElementById("btnStart");
let $btnStop=document.getElementById("btnStop");
let $txtTps=document.getElementById("txtTps");

/*Gestion d'évènements*/
$btnCpt.addEventListener("click", btnStart);
$btnStop.addEventListener("click", stopCpt);

/*variables globales*/
let cpt=0; // gestion d'affichage (setInterval)
let hdep=null; // heure de départ du chrono

/*méthode*/
function btnStart(){
    hdep = new Date();
    cpt = setInterval(affHeure,1);/*moins il y a de zéro plus ça ira vite*/
}

function stopCpt(){
    clearInterval(cpt);
}

function affHeure(){
    let diff = new Date() - hdep;
    let tps = new Date(diff);
/*amélioration de l'affichage avec toString et padStart*/
    let str = tps.getUTCHours().toString().padStart(3,"0") + ":"
        +tps.getUTCMinutes().toString().padStart(2,"0")+":"
        +tps.getUTCSeconds().toString().padStart(2,"0")+"."
        +tps.getUTCMilliseconds().toString().padStart(3,"0");
    txtTps.value = str;
}