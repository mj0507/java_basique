/*new Date() => heure système*/
/*Les dates, afficher la date actuelle*/
let maintenant = new Date();
console.log('maintenant :>>', maintenant);

/*Les dates peuvent être complétées avec l'heure etc*/
/*Attention, car les mois commencent à zéro, c'est pour cela qu'octobre est à 9 et non pas à 10*/
let dateExam = new Date(2023, 9, 12, 9, 10);
console.log('dateExam :>>', dateExam);

/*Le chrono entre heure depart et heure fin */
//let hdep = new Date();
//for (let i = 0; i < 365217; i++) {
//}
//let hfin = new Date();
//console.log("durée : ", hfin - hdep); /*sera affiché en secondes*/

/*Afficher le chrono en passant la valeur dans new Date()*/
let tps = new Date(365217);
console.log('tps :>>', tps);
console.log(tps.getUTCHours() + "h",
            tps.getUTCMinutes() + "m",
            tps.getUTCSeconds() + "s",
            tps.getUTCMilliseconds() + "ms")



/*Exercice Chrono*/
/*Variable pour simplifier le code*/
let $btnCpt=document.getElementById("btnStart");
let $btnStop=document.getElementById("btnStop");
let $txtTps=document.getElementById("txtTps");

/*Gestion d'évènements*/
$btnCpt.addEventListener("click", btnStart);
$btnStop.addEventListener("click", stopCpt);

/*variables globales*/
let cpt=0; // gestion d'affichage (setInterval)
let hdep=null; // heure de départ du chrono

/*méthode*/
function btnStart(){
    hdep = new Date();
    cpt = setInterval(affHeure,1);/*moins il y a de zéro plus ça ira vite*/
}

function stopCpt(){
    clearInterval(cpt);
}

function affHeure(){
let diff = new Date() - hdep;
let tps = new Date(diff);

    let str = tps.getUTCHours() + ":"
                    +tps.getUTCMinutes()+":"
                    +tps.getUTCSeconds()+"."
                    +tps.getUTCMilliseconds();
txtTps.value = str;
}