/*Afficher les nombres*/
let nbres = [7, 2, 11, 3, 4, 17, 14, 6];
console.log('nbres :>>', nbres);

// Afficher les nombres paires
let nbresPairs1 = [];//Partir d'un tableau vide
for (let i = 0; i < nbres.length; i++)//tant que plus petit que le nombres se trouvant dans le tableau
{
    if (nbres[i] % 2 == 0) {
        nbresPairs1.push(nbres[i]);
    }
}
console.log('nbresPairs1 :>>', nbresPairs1);

//Simplification du code par rapport au dessus
let nbresPairs2=[];
for(let x of nbres)
    if(x % 2 == 0) {
        nbresPairs2.push(x);
    }
console.log('nbresPairs2 :>>', nbresPairs2);

// Il existe une fonction pour encore alléger le code .filter
let nbresPairs3= nbres.filter(x=> x % 2 == 0)
console.log('nbresPairs3 :>>', nbresPairs3);


// Afficher les nombres doubles multiplier par eux même
let nbresDoubles = [];
for (let x of nbres) {
    nbresDoubles.push(x * 2);
}
console.log('nbresDoubles :>>',nbresDoubles);
// Encore plus simple avec map

let nbresDoubles2 = nbres.map((x) => x *2);
console.log('nbresDoubles2 :>>', nbresDoubles2);



// La somme des Nombres
let sommeNbres = 0;
for (let x of nbres) {
    sommeNbres= sommeNbres + x ;
}
    console.log('sommeNbres :>>', sommeNbres);

// Plus simple avec reduce
// /*Somme des nombres, initialisé à zéro*/
const sommeNbres2 = nbres.reduce((s,x)=> s + x, 0);/*x représente chacun des éléments*/
console.log('sommeNbres2 :>>', sommeNbres2);

//Afficher le nombre le plus grand
let maxNbres =nbres[0];/*Premier nombre*/
for(let x of nbres) {
    if(x > maxNbres){
        maxNbres=x;
    }
}
    console.log('maxNbres :>>',maxNbres);


// plus simple avec le reduce
const maxNbres2 = nbres.reduce((m,x)=> (x > m ? x : m), nbres[0]);
console.log('maxNbres2 :>>', maxNbres2);


//Utilisation de Math.max
const maxNbres3 = Math.max(...nbres);
console.log('maxNbres3 :>>', maxNbres3);

// Afficher un tableau trier ATTENTION SORT MODIFIE LE TABLEAU qui triera la suite de code
//Commencer donc par faire une copie du tableau[...] pour pouvoir l'utiliser n'importe où
const tabTri= [...nbres].sort((x,y)=>x-y);
console.log('Tableau trié :>>',tabTri)
/*COPIE D'UN TABLEAU
let tab = [1,2,3]; 1 tableau
[...tab]=>1,2,3; 3 valeurs */