/*Variable pour simplifier le code*/
let $btnCpt=document.getElementById("btnStart");
let $btnStop=document.getElementById("btnStop");
let $btnReset=document.getElementById("btnReset");
let $txtTps=document.getElementById("txtTps");

/*Gestion d'évènements*/
$btnCpt.addEventListener("click", btnStart);
$btnStop.addEventListener("click", stopCpt);
$btnReset.addEventListener("click", reset);

/*variables globales*/
let cpt=null; // gestion d'affichage (setInterval)
let hdep=null; // heure de départ du chrono

/*méthode*/
function btnStart(){
    if(cpt !=null) return; // Si cpt différent de null ne fait rien car chrono déjà démarré
    hdep = new Date();
    cpt = setInterval(affHeure,1);/*moins il y a de zéro plus ça ira vite*/
}

function stopCpt(){
    if(cpt == null) return; //chrono non démarré

    clearInterval(cpt);
    cpt = null;
}

function reset() {
    if(cpt != null) return;//chrono démarré
    $txtTps.value = "000:00:00:000";
}

function affHeure(){
    let diff = new Date() - hdep;
    let tps = new Date(diff);
    /*amélioration de l'affichage avec toString et padStart*/
    let str = tps.getUTCHours().toString().padStart(3,"0") + ":"
        +tps.getUTCMinutes().toString().padStart(2,"0")+":"
        +tps.getUTCSeconds().toString().padStart(2,"0")+"."
        +tps.getUTCMilliseconds().toString().padStart(3,"0");
    txtTps.value = str;
}