// Les objets
let p1 = {nom: "Dubois",    age: 25, taille: 173};
let p2 = {nom: "Dubalais",  age: 27, taille: 163, sexe:'M'};
let p3 = {nom: "Dupond",    age:45,  taille: 193};



console.log('p1.nom :>>', p1.nom);
let joueurs = [p1, p2, p3];

// Création d'un objet
let equip = {capitaine: p1,
                joueurs: [p1, p2, p3],
                mascotte:{ nom: "ODOR", taille: 120}}
console.log('equip.capitaine.nom :>>', equip.capitaine.nom);

// trouver le plus grand joueur
let grandsJoueurs = equip.joueurs.filter(x => x.taille >= 170); // l'équipe des joueurs filtrée par taille
// supérieure à 170
console.log('GrandJoueurs :>>', grandsJoueurs);

//Afficher
console.log('p1.age :>>', p1.age);
console.log('p3.age :>>', p3.age);
console.log('p2.sexe :>>', p2.sexe);
console.log('p1.sexe :>>', p1.sexe);
console.log('equip.mascotte.taille :>>', equip.mascotte.taille);
console.log('equip?.mascotte?.taille :>>', equip?.mascotte?.taille);

/*pour vérifier si ok*/
if(equip && equip.mascotte && equip.mascotte.taille)
    console.log(equip.mascotte.taille);
else {
    console.log("AÏE");
}

let tailleMascotte = equip?.mascotte?.taille || 999;
console.log('tailleMascotte :>>', tailleMascotte);


//------------------------------------------------------------------