/*Ajouter*/

// Variable pour simplifier le code
let $txtNom = document.getElementById('txtNom');// nom id
let $cboEquipe = document.getElementById('cboEquipe'); // nom id

// Création évènement pour btnAjout au click qui permettra d'ajouter au click
document.getElementById("btnAjout").addEventListener('click', ajouter)


function ajouter() {
    let nom = $txtNom.value;
    if (nom != "") // Si nom vide
    {
        console.log("j'ajoute " + nom);
        // Construction de l'option pour ajouter
        let $opt = document.createElement("option");
        $opt.innerHTML = nom;
        $opt.value= nom;
        // Ajout à la liste et afficher
        $cboEquipe.appendChild($opt);
        //afficher la liste
        console.log('$cboEquipe.children() :>>' , $cboEquipe.children);
    }
    $txtNom.value = "";// Remettre à zéro la zone du formulaire
    $txtNom.focus();// revenir sur la zone du formulaire
}
