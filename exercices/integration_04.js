/*Supprimer*/
"use strict"

// Création de variables pour simplifier le code
let $txtNom = document.getElementById('txtNom');// nom id qui vient du html
let $ulEquipe = document.getElementById('ulEquipe'); // nom id qui vient du html

// Création évènement pour btnAjout au click qui permettra d'ajouter au click
document.getElementById("btnAjout").addEventListener('click', ajouter)


function ajouter() {
    let nom = $txtNom.value; /*Lire ce qui se trouve dans nom et sa valeur*/
    if (nom != "") // Si nom pas vide
    {
        console.log("j'ajoute " + nom);
        // Construction de l'élément li
        let $li = document.createElement("li");
        $li.innerHTML = nom;

        //construction bouton pour pouvoir l'ajouter
        let $btn = document.createElement("button")
        $btn.innerHTML = "supprimer";
        //Création
        $btn.addEventListener('click', function (){

            //console.log("coucou"); pour afficher coucou dans la console
            // patricide
            this.parentElement.remove();//Supprimer le parent de l'élément $btn, c'est à dire le li
                                        // this représente l'objet dans lequel on est
    })
        //Ajouter le bouton à l'élément li
        $li.appendChild($btn);

        // Ajout à la liste et afficher
        $ulEquipe.appendChild($li);


        console.log('$ulEquipe.children() :>>', $ulEquipe.children);
    }
    $txtNom.value = "";// Remettre à zéro la zone du formulaire
    $txtNom.focus();// revenir sur la zone du formulaire
}
