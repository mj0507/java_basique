document.getElementById('nbr1').addEventListener('keyup',calculer);/*Ne pas mettre de parenthèse pour la fonction
 calculer()*/
document.getElementById('nbr2').addEventListener('keydown',calculer);/*id doit être unique*/

//Créer une fonction permettant d'aller chercher les valeurs venant du formulaire html
function calculer(){
    let nbr1 = document.getElementById('nbr1').value;/*.value récupère la valeur d'un élément*/
    // Permet d'aller rechercher la valeur de input du nbr1 de integration_02.html
    let nbr2 = document.getElementById('nbr2').value;

        /*Changer le text en valeur numérique*/
        nbr1 = parseInt(nbr1);/*document.getElementById('nbr1').value; se mettrait alors dans les parenthèses*/
        nbr2 = parseInt(nbr2);

    let resultat = nbr1 + nbr2;
    document.getElementById('resultat').value = resultat;

}