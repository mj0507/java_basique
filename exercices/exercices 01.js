//Boucles for
// Création d'une constante pour éviter de mettre des valeurs numériques dans nos boucles, et éviter
// de devoir changer les valeurs manuellement dans chaque exercice.
const max=10;
//1/Afficher les nombres de 1 à 10
console.log("Exercice 1")
for(let i=1; i<=max;i++){
    console.log(i);
}

//2/Afficher les nombres de 10 à 0 (10,9,8,7,6,5,4,3,2,1)
console.log("Exercice 2")
for(let i=max; i>=0; i--){
console.log(i);
}

//3/Afficher les nombres impairs jusqu'à 10 (1,3,5,7,9)
console.log("Exercice 3")
for(let i=1; i<=max; i+=2){
    console.log(i)
}

//4/Afficher 10 premiers nombres impairs (1,3,5,7,9,11,13,15,17,19)
console.log("Exercice 4")
for(let i=1; i<=max; i++){
    console.log(i*2-1);
}

//5/ Afficher les carrés des nombres de 1 à 10 (1*1=1 2*2=4...10*10=100)
console.log("Exercice 5")
for(let i=1; i<=max; i++ ){
    console.log(i,'*',i,'=',i*i);
    console.log(i+" * "+i+" = "+i*i);
    console.log(`${i}*${i}=${i*i}`)
}

//6/ Afficher le nombre/complément de 1 à 10(1+5; 2+8; 3+7...9+1;10+0)
console.log("Exercice 5")
for(let i=1; i<=max; i++ ){
    console.log(i,"+",max-i);
}