//Boucles for
// Création d'une constante pour éviter de mettre des valeurs numériques dans nos boucles, et éviter
// de devoir changer les valeurs manuellement dans chaque exercice.
const max = 10;
//1/Afficher les nombres de 1 à 10 sauf le 7.
console.log("Exercice 1")
for (let i = 1; i <= max; i++) {
    if (i != 7) {//Si i différent de 7.
        console.log(i);//Affiche i
    }
}

//2/Afficher les nombres de 1 à 10 sauf les multiples de 3.(utilisation du modulo)
console.log("Exercice 2")
for (let i = 1; i <= max; i++) {
    if (i % 3 != 0) { //Si i est multiple de 3 et différent de zéro
        console.log(i);//Affiche i
    }
}


let tab = [9, 2, 5, 4, -7, 6, 0, -4];/*Utilisation du tab pour les prochains exercices*/

console.log("Exercice 3")
console.log('tab.length :>>', tab.length); // Renvoie la taille du tableau dans notre cas 8
//3/Afficher les nombres positifs(>0) de tab 9,2,5,4,6,0
for (let i = 0; i < tab.length; i++) {
    if (tab[i] >= 0) {
        console.log(tab[i]);
    }
}

//console.log(tab.filter((x)=>x>=0)); Possibilité d'afficher plus simplement via filter

//4/Afficher la moyenne de tab
console.log("Exercice 4")

//Calcul du total
let total = 0;
for (let i = 0; i < tab.length; i++) {
    total = total + tab[i];
}
console.log("tab:>>", total);

//Calcul de la moyenne (total/nombres d'éléments)
let moyenne = total / tab.length;
console.log("moyenne:>>", moyenne);
if (tab.length > 0) {
    console.log("moyenne :" + total / tab.length);
} else {
    console.log("pas de moyenne possible");
}