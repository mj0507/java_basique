"use strict";
// Variable pour simplifier le code
const $txtTaches = document.getElementById("txtTache");// nom id
const $ulTaches = document.getElementById("ulTaches");// nom id
const $btnAjout = document.getElementById("btnAjout");// nom id
const $chkCacher = document.getElementById("chkCacher");//nom id

// Création évènement pour btnAjout au click qui permettra d'ajouter au click
$btnAjout.addEventListener("click", ajouter)
// au click afficher les tâches
$chkCacher.addEventListener("click", afficherListTaches);
//Variables globales pour la gestion
const tabTaches = [];


function ajouter() {
    let nomTache = $txtTaches.value;
    nomTache = nomTache.trim();// Afin d'enlever les espaces avant et après
    //Si le nom de la tâche est vide rien ne se passe, retourne à vide.
    if (nomTache == "") return;
    //Si déjà présent dans le tableau=> Gestion des erreurs
    if (tabTaches.findIndex(x => x.nom == nomTache) >= 0) return;// Permet de retrouver l'élément afin de ne pas
    // l'afficher en double


    //Ajouter la nouvelle tâche dans le tableau
    tabTaches.push({nom: nomTache, terminee: false})

    afficherListTaches();

    //Se préparer pour la nouvelle saisie
    $txtTaches.value = "";
    $txtTaches.focus();

    console.log('tabTaches:>>', tabTaches); // Afficher taTaches dans la console pour voir si ça fonctionne

}

function afficherListTaches() {//Pour afficher la liste des tâches


    //vider la ul
    while ($ulTaches.hasChildNodes()) {// Tant que...
        $ulTaches.removeChild($ulTaches.firstChild);//Suppression des enfants en commencant par le 1ER
    }

    //Ajouter une li pour chaque tâche à afficher// Déclaration d'une nouvelle variable
    for (const tache of tabTaches) {
        if ($chkCacher.checked && tache.terminee) continue // Si on doit cacher les éléments

        // Construction de la ligne
        const $li = document.createElement("li");
        $li.innerHTML = tache.nom;

        //construction du button + ajout
        const $chk = document.createElement("input");
        $chk.type = "checkbox";// Va rechercher le checkbox du input
        $chk.checked = tache.terminee;//

        //ajouter un attribut afin de pouvoir modifier le true ou false
        $chk.setAttribute("NomTache", tache.nom)
        $chk.addEventListener("click", function () {
            let nomTache = this.getAttribute("nomTache");


            tabTaches.find(x => x.nom == nomTache).terminee = this.checked//find renvoie l'objet lui mm
            //console.log('tabTaches:>>', tabTaches);

            afficherListTaches();// Afficher la liste
        });


        $li.insertBefore($chk, $li.firstChild);

        //Ajouter à la ul
        $ulTaches.appendChild($li);

    }
}

