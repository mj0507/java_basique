const max = 10;

// 1. Afficher les nombres de 1 à 10  => 1 2 3 4 5 6 7 8 9 10
console.log("----------Exercice 1--------------");
for (let i = 1; i <= max; i++) {
  console.log(i);
}

// 2. Afficher les nombres de 10 à 0 => 10 9 8 7 6 5 4 3 2 1 0
console.log("----------Exercice 2--------------");
for (let i = max; i >= 0; i--) {
  console.log(i);
}

// 3. Afficher les nombres impairs jusque 10 => 1 3 5 7 9
console.log("----------Exercice 3--------------");
for (let i = 1; i <= max; i = i + 2) {
  console.log(i);
}

// 5. Afficher les carrés des nombres de 1 à 10 => 1*1=1 2*2=4 3*3=9 ... 10*10=100
console.log("----------Exercice 5--------------");
for (let i = 1; i <= max; i++) {
  console.log(i, "*", i, "=", i * i);
  console.log(i + " * " + i + " = " + i * i);
  console.log(`${i} * ${i} = ${i * i}`);
}

// 4. Afficher les 10 premiers nombres impairs => 1 3 5 7 9 11 13 15 17 19
console.log("----------Exercice 4--------------");
for (let i = 1; i <= max; i++) {
  console.log(i * 2 - 1);
}

// 6. Afficher les nombres plus leur complément à 10 => 1+9 2+8 3+7 ... 9+1 10+0
console.log("----------Exercice 6--------------");
for (let i = 1; i <= max; i++) {
  console.log(i, "+", max - i);
}
