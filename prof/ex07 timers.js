"use strict";

let $btnCpt = document.getElementById("btnCpt");
let $btnStop = document.getElementById("btnStop");

$btnCpt.addEventListener("click", startCpt);
$btnStop.addEventListener("click", stopCpt);

let cpt = 0;
let timer1 = null;

function startCpt() {
  if (timer1 != null) return;

  console.log("start...");
  timer1 = setInterval(incCpt, 1000);
}

function stopCpt() {
  clearInterval(timer1);
  timer1=null;
}

function incCpt() {
  cpt++;
  $btnCpt.innerHTML = cpt;
  console.log("cpt :>> ", cpt);
}
