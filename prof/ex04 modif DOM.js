// variables pour simplifier le code (alias)
let $txtNom = document.getElementById("txtNom");
let $cboEquipe = document.getElementById("cboEquipe");

document.getElementById("btnAjouter").addEventListener("click", ajouter);

function ajouter() {
  let nom = $txtNom.value;

  if (nom != "") {
    //console.log("j'ajoute " + nom);
    // construction de l'option
    let $opt = document.createElement("option");
    $opt.innerHTML = nom;
    $opt.value = nom;
    // ajout à la liste
    $cboEquipe.appendChild($opt);
    console.log('$cboEquipe.children() :>> ', $cboEquipe.children);
  }

  $txtNom.value = "";
  $txtNom.focus();
}
