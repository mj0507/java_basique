const max = 10;

// 1. Afficher les nombres de 1 à 10  sauf 7 => 1 2 3 4 5 6 8 9 10
console.log("----------Exercice 1--------------");
for (let i = 1; i <= max; i++) {
  if (i != 7) {
    console.log(i);
  }
}

// 2. Afficher les nombres de 1 à 10 sauf les multiples de 3 => 1 2 4 5 7 8 10
console.log("----------Exercice 2--------------");
for (let i = 1; i <= max; i++) {
  if (i % 3 != 0) {
    console.log(i);
  }
}
// ------------------------------------------------------------------
let tab = [9, 2, 5, 4, -7, 6, 0, -4];
//let tab = [];
console.log("tab.length :>> ", tab.length);

// 3. Afficher les nombres positifs (>=0) de tab
console.log("----------Exercice 3--------------");
for (let i = 0; i < tab.length; i++) {
  if (tab[i] >= 0) {
    console.log(tab[i]);
  }
}
// console.log(tab.filter((x) => x >= 0));

console.log("----------Exercice 4--------------");

// 4. Afficher la moyenne de tab
// calcul du total
let total = 0;
for (let i = 0; i < tab.length; i++) {
  total = total + tab[i];
}
console.log("total :>> ", total);

// calcul de la moyenne (total / nbre d'éléments)
//let moyenne = total / tab.length;
//console.log("moyenne :>> ", moyenne);

if (tab.length > 0) {
  console.log("moyenne : ", total / tab.length);
} else {
  console.log("pas de moyenne possible");
}
