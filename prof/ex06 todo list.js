"use strict";

// variables pour simplifier le code (alias)
const $txtTache = document.getElementById("txtTache");
const $ulTaches = document.getElementById("ulTaches");
const $btnAjouter = document.getElementById("btnAjouter");
const $chkCacher = document.getElementById("chkCacher");

$chkCacher.addEventListener("click", afficherListeTaches);


// variables globales pour la gestion
const tabTaches = [];

// fonction pour ajouter une tâche
$btnAjouter.addEventListener("click", ajouterTache);

function ajouterTache() {
  let nomTache = $txtTache.value;
  nomTache = nomTache.trim();

  //gestion des cas d'erreur
  if (nomTache == "") return;
  if (tabTaches.findIndex((x) => x.nom == nomTache) >= 0) return;

  //ajouter la nouvelle tâche dans le tableau
  tabTaches.push({ nom: nomTache, terminee: false });

  afficherListeTaches();

  //se préparer pour la nouvelle saisie
  $txtTache.value = "";
  $txtTache.focus();
}

function afficherListeTaches() {
  //vider la ul
  //$ulTaches.innerHTML = "";
  while ($ulTaches.hasChildNodes()) {
    $ulTaches.removeChild($ulTaches.firstChild);
  }

  //ajouter une li pour chaque tâche à afficher
  for (const tache of tabTaches) {
    if($chkCacher.checked && tache.terminee) continue;


    // construction de la ligne
    const $li = document.createElement("li");
    $li.innerHTML = tache.nom;

    // ajout de la case à cocher
    const $chk = document.createElement("input");
    $chk.type = "checkbox";
    $chk.checked= tache.terminee;
    $chk.setAttribute("nomTache", tache.nom);
    $chk.addEventListener("click", function () {
      let nomTache = this.getAttribute("nomTache");
      tabTaches.find((x) => x.nom == nomTache).terminee = this.checked;
      afficherListeTaches();
    });

    $li.insertBefore($chk, $li.firstChild);

    // ajouter à la ul
    $ulTaches.appendChild($li);
  }
}
